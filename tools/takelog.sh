#!/bin/bash
cd $(dirname $0)
ruby stat.rb /api/rooms /rooms/ /img/ /css/rc-color-picker.css /css/sanitize.css /bundle.js /api/stream/rooms/ /api/strokes/rooms/ < /var/log/nginx/main_access.log | tee stat.txt

# /usr/local/go/bin/go tool pprof ~/isubata/webapp/go/isubata /tmp/cpu.prof <<EOF
# list main.* > prof.txt
# EOF
