package main

import (
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"./slack"
)

var escaper = strings.NewReplacer(
	`&`, "&amp;",
	`<`, "&lt;",
	`>`, "&gt;",
)
var unescaper = strings.NewReplacer(
	"&amp;", `&`,
	"&lt;", `<`,
	"&gt;", `>`,
)

func ExecuteCommand(bashscript string) (string, error) {
	cmd := exec.Command("/bin/bash", "-s")
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return "", err
	}
	stdin.Write([]byte(bashscript))
	stdin.Close()
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		return string(stdoutStderr), err
	}
	return string(stdoutStderr), nil
}

func handleResponse(rtm *slack.RTM, ev *slack.MessageEvent, text string) {
	startTyping(rtm, ev.Channel)

	cmd := unescaper.Replace(text)
	log.Printf("Command: %s\n", cmd)
	res, err := ExecuteCommand(cmd)
	if err != nil {
		res = res + "\n" + err.Error() // exit status %d
	}

	if res == "" {
		res = "(no output)"
	}
	var decoratedResult string
	if mentionWhenReply {
		decoratedResult = "<@" + ev.User + "> " + escaper.Replace(res)
	} else {
		decoratedResult = escaper.Replace(res)
	}
	if len(decoratedResult) > 4000 {
		_, err := rtm.UploadFile(slack.FileUploadParameters{Content: res, Title: "output_" + BotID + ".txt", InitialComment: "BOTID=" + BotID, Channels: []string{ev.Channel}})
		if err != nil {
			log.Println(err)
		}
	} else {
		log.Println("Send: ", decoratedResult)
		response := rtm.NewOutgoingMessage(decoratedResult, ev.Channel)
		response.ThreadTimestamp = ev.Timestamp // reply as a thread
		response.ReplayBroadcast = true

		rtm.SendMessage(response)
	}
	stopTyping(rtm, ev.Channel)
}

const verbose = true
const mentionWhenReply = false

var typing struct {
	sync.Mutex
	waiting int
}

func startTyping(rtm *slack.RTM, channel string) {
	typing.Lock()
	defer typing.Unlock()
	typing.waiting++
	rtm.SendMessage(rtm.NewTypingMessage(channel))
}
func stopTyping(rtm *slack.RTM, channel string) {
	typing.Lock()
	defer typing.Unlock()
	typing.waiting--
	if typing.waiting > 0 {
		rtm.SendMessage(rtm.NewTypingMessage(channel)) // typing...
	}
}

var BotID = strconv.FormatInt(time.Now().UnixNano(), 36)

func main() {
	os.Setenv("BOTID", BotID)
	apikey := os.Getenv("SLACK_API_KEY")
	if apikey == "" {
		log.Panicln("SLACK_API_KEY is not set")
	}

	api := slack.New(apikey)

	rtm := api.NewRTM()
	go rtm.ManageConnection()

	var botId string
	var botName string
	for msg := range rtm.IncomingEvents {
		switch ev := msg.Data.(type) {
		case *slack.ConnectedEvent:
			botId = ev.Info.User.ID
			botName = ev.Info.User.Name
			log.Printf("My name is %s and ID is %s\n", botId, botName)

		case *slack.MessageEvent:
			if verbose {
				log.Printf("event: %#v\n", ev)
			}
			text := ev.Text
			expectedPrefix := "<@" + botId + ">"
			if ev.Type == "message" && strings.HasPrefix(text, expectedPrefix) && ev.User != botId {
				user, err := api.GetUserInfo(ev.User)
				if err != nil {
					log.Println(err)
					user = &slack.User{}
				}
				log.Printf("Received '%s' from %s\n", text, user.Name)
				go handleResponse(rtm, ev, text[len(expectedPrefix):])
			}

		default:
			if verbose {
				log.Printf("event: %#v\n", ev)
			}
		}
	}
}
