#!/usr/bin/sudo /bin/bash

cd $(dirname $0)
source ./init_header.sh

echo '### nginx ###'
update_if_differ nginx .///etc/nginx/sites-enabled/nginx.conf /etc/nginx/sites-enabled/nginx.conf
#update_if_differ nginx .///etc/nginx/mime.types /etc/nginx/mime.types
update_if_differ nginx .///etc/nginx/nginx.conf /etc/nginx/nginx.conf
update_if_differ nginx .///webapp/ssl/oreore.crt /etc/nginx/isucon_cert.crt
update_if_differ nginx .///webapp/ssl/oreore.key /etc/nginx/isucon_cert.key

ensure_nginx_syntax
restart_service_if_updated nginx

echo '### mysql ###'
update_if_differ mysql .///etc/mysql/mysql.conf.d/mysqld_safe_syslog.cnf /etc/mysql/mysql.conf.d/mysqld_safe_syslog.cnf
update_if_differ mysql .///etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
update_if_differ mysql .///etc/mysql/conf.d/mysqldump.cnf /etc/mysql/conf.d/mysqldump.cnf
update_if_differ mysql .///etc/mysql/conf.d/mysql.cnf /etc/mysql/conf.d/mysql.cnf
update_if_differ mysql .///etc/mysql/my.cnf /etc/mysql/my.cnf
ensure_mysql_syntax
restart_service_if_updated mysql

echo '### isucon.service ###'
(
	cd ./webapp/go
	make
)
(
	cd ./webapp/react
	make
)
#update_if_differ isubata.golang.service ./public /home/isucon/isubata/webapp/public
#update_if_differ isubata.golang.service ./go/ /home/isucon/isubata/webapp/go/
update_if_differ isucon.service .///etc/systemd/system/isucon.service /etc/systemd/system/isucon.service
systemctl --quiet is-enabled isucon.service || ( set -x; systemctl --now enable isucon.service )
restart_service isucon.service
update_if_differ react.service .///etc/systemd/system/react.service /etc/systemd/system/react.service
systemctl --quiet is-enabled react.service || ( set -x; systemctl --now enable react.service )
restart_service react.service

#echo '### other ###'
#update_if_differ other ./etc/security/limits.conf /etc/security/limits.conf
#update_if_differ other ./etc/sysctl.conf /etc/sysctl.conf

# mysql index
mysql <<< 'use isuketch; ALTER TABLE points ADD INDEX stroke_id_id_idx(stroke_id, id)' || true
mysql <<< 'use isuketch; ALTER TABLE strokes ADD INDEX room_id_id_idx(room_id, id)' || true
