#!/usr/bin/sudo /bin/bash

set -xu

curl -k https://app1/initialize
curl -k https://app2/initialize
curl -k https://app3/initialize
cd $(dirname $0)/bench
./local-bench -urls=https://app2:443 -timeout 30 2>&1 | tee ../log/$(../logger.sh lastid)/bench.txt

